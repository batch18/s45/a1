import React from 'react';

import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function CourseCard(){

	return(

		<Row className="px-3 mt-3">
			<Col>
				<Card>
					<Card.Body>
						<Card.Title>Sample Course</Card.Title>
						<Card.Text>
							<p>
							Description : <br/>
							This is a sample course offering
							</p>
							<p>
							Price : <br/>
							Php: 40,000
							</p>
						</Card.Text>
				    	<Button variant="primary">Go somewhere</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}