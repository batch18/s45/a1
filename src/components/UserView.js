import React, {useState, useEffect}from 'react'

import {Container} from 'react-bootstrap';

import Course from './Courses'

export default function UserView({courseData}){


	const [courses, setCourses] = useState([])

	useEffect( () =>{

		const courseArr = courseData.map( (course)=> {

			if(course.isActive === true){
				return <Course key={course._id} courseProp={course}/>
			}
			else{
				return null
			}
		})

		setCourses(courseArr)

	},[courseData])


	return(
		<Container>
			{courses}

		</Container>

	)
}