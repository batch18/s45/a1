import React, {Fragment, useContext}from 'react';
import {NavLink, Link, useHistory} from 'react-router-dom'

import UserContext from './../UserContext';

/*react bootstrap*/
import {Navbar, Nav} from 'react-bootstrap';


/*app navbar*/
export default function AppNavbar(){

  /*console.log(props)

  let user = props.user*/

  let history = useHistory()

  const {user, unsetUser} = useContext(UserContext);

  const logout = () => {
    unsetUser();
    history.push('/Login');
  }

  
  let leftNav = (user.id === null) ?
    (user.isAdmin === true) ?
      <Fragment>
        <Nav.Link as={NavLink} to="/addCourse">Add Course</Nav.Link>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
      </Fragment>
      :
      <Fragment>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
      </Fragment>
    :
    <Fragment>
      <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
    </Fragment>



  return (

      <Navbar bg="info" expand="lg">
        <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/Courses">Courses</Nav.Link>
          </Nav>
          <Nav>
            {leftNav}
          </Nav>
        </Navbar.Collapse>
      </Navbar>

    )
}