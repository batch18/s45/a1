import React, {useState, useEffect} from 'react'

import {Container} from 'react-bootstrap'

export default function Counter() {

	const [count, setCount] = useState(0);

	useEffect( () => {

		document.title = `you clicked ${count} times`

	}, [count]);	

	return(
		<Container>
			<h1>You clicked {count} Times</h1>
			<button className="btn btn-primary" onClick={()=>{ setCount(count + 1) }} >Click Me</button>

		</Container>
	)

}