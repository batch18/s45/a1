import React from 'react';

import {Container, Row, Col, Jumbotron} from 'react-bootstrap';

export default function ErrorPage() {
	return (


		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-3">
					  <h1>Error 404</h1>
						<p>Page Not Found</p>
					  <p>
					    <a href="/">Return to home</a>
					  </p>
					</Jumbotron>
				</Col>	
			</Row>
		</Container>
	)
}


