import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

import {Card, Button} from 'react-bootstrap'


export default function Course({courseProp}){

	const {name, description, price, _id} = courseProp


	return (

			<Card className="mt-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>
						<p>
						Description : <br/>
						{description}
						</p>
						<p>
						Price : <br/>
						{price}
						</p>
						
						<Link className="btn btn-primary" to={`/Courses/${_id}`}>Details</Link>
					</Card.Text>
				</Card.Body>
			</Card>



		)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
		
}